# Techdoc


## Getting started

![Pocket Awakening](https://gitlab.com/questionabledestiny/pokemon-art/-/raw/191126_release/UI/resources/activity/banner_activity_zhongqiu.png?ref_type=heads)
You'll need 4 repos:

- https://gitlab.com/questionabledestiny/pokemon-apk
- https://gitlab.com/questionabledestiny/pokemon-art
- https://gitlab.com/questionabledestiny/pokemon-server
- https://gitlab.com/questionabledestiny/pokemon-server-tool

There's a fifth repo filled with various powerpoints and instructions from the original developers

- https://gitlab.com/questionabledestiny/techdoc

